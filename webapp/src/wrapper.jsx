const {PostUtils} = window;

const appendPostStyle = (divId, color) => {
    const css = `#${divId} { background: ${color}; }`;
    const head = document.head || document.querySelector('head');
    const style = document.createElement('style');
    style.appendChild(document.createTextNode(css));
    head.appendChild(style);
};

const wrapper = (registry, specificTeam, state, users) => {
    class PostTypeFormatted extends window.React.PureComponent {
        static previousUserId = null;
        static previousDate = null;
        static previousEpochTime = null;

        static customReset(userId, date, epochTime) {
            PostTypeFormatted.previousUserId = userId;
            PostTypeFormatted.previousDate = date;
            PostTypeFormatted.previousEpochTime = epochTime;
        }

        static customCanShowRoles(userId, date, epochTime) {
            const prevId = PostTypeFormatted.previousUserId;
            const prevDate = PostTypeFormatted.previousDate;
            const prevEpochTime = PostTypeFormatted.previousEpochTime;

            PostTypeFormatted.customReset(userId, date, epochTime);

            if (userId !== prevId) {
                return true;
            }
            if (date !== prevDate) {
                return true;
            }
            const fiveMinutes = 5 * 60 * 1000;
            if (epochTime - fiveMinutes > prevEpochTime) {
                return true;
            }

            return false;
        }

        render() {
            const post = this.props.post;
            const formattedText = PostUtils.formatText(post.message, {
                mentionHighlight: true,
                atMentions: true,
                siteURL: state.entities.general.config.SiteURL,
                team: specificTeam,
            });

            const epochTimeCreateAt = post.create_at;
            const timePosted = (new Date(epochTimeCreateAt)).toLocaleString();
            const datePosted = timePosted.split(', ')[0];
            const userId = post.user_id;
            const user = users[userId];

            const roleStyle = {
                fontSize: '10px',
                fontWeight: 'bold',
                marginRight: '5px',
                padding: '3px',
                borderRadius: '5px',
            };

            const getBadgeStyle = (badge) => {
                return {
                    ...roleStyle,
                    background: badge.bgColor,
                    color: badge.textColor,
                }
            };

            if (user && user.postBgColor) {
                appendPostStyle(`post_${post.id}`, user.postBgColor);
            }

            const canShowRoles = PostTypeFormatted.customCanShowRoles(userId, datePosted, epochTimeCreateAt);
            const badges = user && canShowRoles ? user.badges : [];

            return (
                <div>
                    {
                        canShowRoles ? (<div style={{ marginBottom: '5px' }}>
                            {
                                badges.map((badge) =>
                                    (
                                        <span
                                            key={Math.random()}
                                            style={getBadgeStyle(badge)}
                                        >{badge.name}</span>
                                    ),
                                )
                            }
                        </div>) : (<div/>)
                    }
                    { PostUtils.messageHtmlToComponent(formattedText) }
                </div>
            );
        }
    }
    registry.registerPostTypeComponent('', PostTypeFormatted);
};

export default wrapper;