import {id as pluginId} from './manifest';

import UserAttribute from './components/user_attribute';
import CustomAttributesSettings from './components/admin_settings/custom_attribute_settings.jsx';
import Reducer from './reducers';

import wrapper from './wrapper';

const sample = (registry, store) => {
    const {teams} = store.getState().entities.teams;
    let specificTeam = null;
    for (const key of Object.keys(teams)) {
        specificTeam = teams[key];
        break;
    }

    const promise = fetch(`/plugins/${pluginId}/users/attributes`);
    promise.then((res) => res.json()).then(({CustomAttributes}) => {
        const users = {};
        for (const customAttribute of CustomAttributes) {
            for (const userId of customAttribute.UserIDs) {
                if (!users[userId]) {
                    users[userId] = {
                        badges: [],
                        postBgColor: '',
                    };
                }
                const badge = {
                    name: customAttribute.Name,
                    bgColor: customAttribute.BadgeBgColor,
                    textColor: customAttribute.BadgeTextColor,
                };
                users[userId].badges.push(badge);
                users[userId].postBgColor = customAttribute.PostBgColor;
            }
        }
        wrapper(registry, specificTeam, store.getState(), users);
    });
};

export default class Plugin {
    initialize(registry) {
        registry.registerReducer(Reducer);
        registry.registerPopoverUserAttributesComponent(UserAttribute);

        registry.registerAdminConsoleCustomSetting('CustomAttributes', CustomAttributesSettings);

        sample(registry, store);
    }
}

window.registerPlugin(pluginId, new Plugin());
